import pandas as pd

def preprocessing(data):
    
    data = data.astype('category')
    data['Label'] = data['Label'].cat.codes

    # Be careful to consider the order for the attribute "size" 
    size_to_int = {'small': 0,'medium': 1,'large': 2}
    data['Size'] = data['Size'].map(size_to_int)

    data_encoded = pd.get_dummies(data[['Color','Shape']])

    data_processed = pd.concat([data_encoded, data[['Size', 'Label']]], axis = 1)
    
    return data_processed