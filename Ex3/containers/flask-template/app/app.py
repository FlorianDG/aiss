#!/usr/bin/env python
# coding: utf-8

# # Task 3 [OPTIONAL] – How to run all together with docker-compose?

# 1. Pick code from all the Tasks (3.1 - 3.4)
# * File > Download as > python (.py)
# * Replace `containers / 3.2 flask-nginx-frontend / flask-template / app / app.py` by dowloaded file (rename it to `app.py`)
# * Open terminal / Power Shell and move to `containers / 3.2 flask-nginx-frontend`
# * Run `docker-compose up` 

# In[ ]:


from flask import Flask, jsonify, request
import pandas as pd


# In[ ]:


# 3.4
df = pd.read_csv('./data/lfw_people.csv', index_col = 0)

df['target'] = df['target'].astype('category')
target_names = df['target'].cat.categories
df['target'] = df['target'].cat.codes

X = df.iloc[:, :-1]
y = df.iloc[:, -1]

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

from sklearn.decomposition import PCA

pca = PCA(n_components=150, svd_solver='randomized', whiten=True).fit(X_train)

X_train_pca = pca.transform(X_train)
X_test_pca = pca.transform(X_test)


# In[ ]:


# 3.3
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

# Train a SVM classification model
param_grid = {'C': [1e3, 5e3, 1e4, 5e4, 1e5],
              'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1]}
clf = GridSearchCV(SVC(kernel='rbf', class_weight='balanced'), param_grid)
clf = clf.fit(X_train_pca, y_train)

# Best estimator found by grid search
print(clf.best_estimator_)

from sklearn.metrics import confusion_matrix

# Quantitative evaluation of the model quality on the test set
y_pred = clf.predict(X_test_pca)


# In[ ]:


app = Flask(__name__)


# In[ ]:


# 3.1
@app.route('/hello_world')
def hello_world2():
    text = 'Hello, World!'
    return jsonify({'text': text})


# In[ ]:


# 3.2
cache = {'name': 'World'}

@app.route('/')
def hello_world():
    return 'Hello, %s!' % cache['name']

@app.route('/hello')
def hello():
    name = request.args.get('name')
    cache['name'] = name
    text = 'Hello, %s!' % name
    return jsonify({'text': text})


# In[ ]:


# 3.3-1
@app.route('/accuracy')
def accuracy():
    return jsonify({'accuracy': clf.score(X_test_pca, y_test)})


# In[ ]:


# 3.3-2
@app.route('/best_params')
def best_params():
    return jsonify({'best_params': clf.best_params_, 'param_grid': param_grid})


# In[ ]:


# 3.4-1
@app.route('/retrain')
def retrain():
    C = float(request.args.get('C'))
    gamma = float(request.args.get('gamma'))
    
    from sklearn.svm import SVC
    from sklearn.model_selection import GridSearchCV

    clf = SVC(kernel='rbf', class_weight='balanced', C=C, gamma=gamma)
    clf = clf.fit(X_train_pca, y_train)

    y_pred = clf.predict(X_test_pca)

    return jsonify({'accuracy': clf.score(X_test_pca, y_test)})


# In[ ]:


# 3.4-2
@app.route('/retrain_grid')
def retrain_grid():
    C_start = float(request.args.get('C_start'))
    C_end = float(request.args.get('C_end'))
    C_step = float(request.args.get('C_step'))

    gamma_start = float(request.args.get('gamma_start'))
    gamma_end = float(request.args.get('gamma_end'))
    gamma_step = float(request.args.get('gamma_step'))
    
    from sklearn.svm import SVC
    from sklearn.model_selection import GridSearchCV
    
    import numpy as np
    
    param_grid = {'C': np.arange(C_start, C_end + C_step, C_step),
                  'gamma': np.arange(gamma_start, gamma_end + gamma_step, gamma_step)}
    
    clf = GridSearchCV(SVC(kernel='rbf', class_weight='balanced'), param_grid)
    clf = clf.fit(X_train_pca, y_train)

    y_pred = clf.predict(X_test_pca)

    return jsonify({'accuracy': clf.score(X_test_pca, y_test),
                   'best_params': clf.best_params_})


# In[ ]:


app.run(debug=True, use_reloader=False, host='0.0.0.0')


# *Note: To restart the app please interrupt the kernel and execute **all** cells again.*
