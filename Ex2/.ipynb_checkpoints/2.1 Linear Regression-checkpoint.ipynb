{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Task 2.1 – Linear Regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are basically two types of supervised machine learning algorithms: Regression and classification. The former predicts continuous value outputs while the latter predicts discrete outputs. For instance, predicting the price of a house in dollars is a regression problem whereas predicting whether a tumor is malignant or benign is a classification problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Regression Theory\n",
    "The term \"linearity\" in algebra refers to a linear relationship between two or more variables. If we draw this relationship in a two dimensional space (between two variables, in this case), we get a straight line.\n",
    "\n",
    "Let's consider a scenario where we want to determine the linear relationship between the numbers of hours a student studies and the percentage of marks that student scores in an exam. We want to find out that given the number of hours a student prepares for a test, about how high of a score can the student achieve? If we plot the independent variable (hours) on the x-axis and dependent variable (percentage) on the y-axis, linear regression gives us a straight line that best fits the data points, as shown in the figure below.\n",
    "\n",
    "![](https://s3.amazonaws.com/stackabuse/media/linear-regression-python-scikit-learn-1.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We know that the equation of a straight line is basically:\n",
    "\n",
    "```y = mx + b```\n",
    "\n",
    "Where b is the intercept and m is the slope of the line. So basically, the linear regression algorithm gives us the most optimal value for the intercept and the slope (in two dimensions). The y and x variables remain the same, since they are the data features and cannot be changed. The values that we can control are the intercept and slope. There can be multiple straight lines depending upon the values of intercept and slope. Basically what the linear regression algorithm does is it fits multiple lines on the data points and returns the line that results in the least error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Regression with Python Scikit Learn\n",
    "In this section we will see how the Python Scikit-Learn library for machine learning can be used to implement regression functions. We will start with simple linear regression involving two variables.\n",
    "\n",
    "### Simple Linear Regression\n",
    "In this regression task we will predict the percentage of marks that a student is expected to score based upon the number of hours they studied. This is a simple linear regression task as it involves just two variables.\n",
    "\n",
    "\n",
    "To import necessary libraries for this task, execute the following import statements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd  \n",
    "import matplotlib.pyplot as plt  \n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('./data/2_1_student_scores.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's explore our dataset a bit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(25, 2)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Hours</th>\n",
       "      <th>Scores</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>2.5</td>\n",
       "      <td>21</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>5.1</td>\n",
       "      <td>47</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3.2</td>\n",
       "      <td>27</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>8.5</td>\n",
       "      <td>75</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>3.5</td>\n",
       "      <td>30</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Hours  Scores\n",
       "0    2.5      21\n",
       "1    5.1      47\n",
       "2    3.2      27\n",
       "3    8.5      75\n",
       "4    3.5      30"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Hours</th>\n",
       "      <th>Scores</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>count</th>\n",
       "      <td>25.000000</td>\n",
       "      <td>25.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>mean</th>\n",
       "      <td>5.012000</td>\n",
       "      <td>51.480000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>std</th>\n",
       "      <td>2.525094</td>\n",
       "      <td>25.286887</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>min</th>\n",
       "      <td>1.100000</td>\n",
       "      <td>17.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>25%</th>\n",
       "      <td>2.700000</td>\n",
       "      <td>30.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>50%</th>\n",
       "      <td>4.800000</td>\n",
       "      <td>47.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>75%</th>\n",
       "      <td>7.400000</td>\n",
       "      <td>75.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>max</th>\n",
       "      <td>9.200000</td>\n",
       "      <td>95.000000</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "           Hours     Scores\n",
       "count  25.000000  25.000000\n",
       "mean    5.012000  51.480000\n",
       "std     2.525094  25.286887\n",
       "min     1.100000  17.000000\n",
       "25%     2.700000  30.000000\n",
       "50%     4.800000  47.000000\n",
       "75%     7.400000  75.000000\n",
       "max     9.200000  95.000000"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX4AAAEWCAYAAABhffzLAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8vihELAAAACXBIWXMAAAsTAAALEwEAmpwYAAAlIElEQVR4nO3de7hVdb3v8fcnIFmihAgoFxFUUlQUaoki5SFFTfNCnm1o1iG7kOWDWvu4Jdtb257tlk49tcvctUlN9vaS5gVJ9jER1HSX5gK8hsrOlLgESxQBhRT6nj/GmDpZrstYizXm9fN6nvnMMcccl+9c4neO+fv9xveniMDMzOrH+8odgJmZlZYTv5lZnXHiNzOrM078ZmZ1xonfzKzOOPGbmdUZJ34zszrjxG/dRtJLkia3WPc5SY+UK6bulH6W7ZI2S9oo6QlJp5Q7rmKSQtIB5Y7DKpsTv1UlST3LdOrfRsRuQD/gOuA2Sf07c4Ayxm4GOPFbiUkaLelBSRskPSvptKL3HpT0xaLXO/xaSK9mz5e0HFiuxPclrZP0uqSnJB3ayjnPktTUYt3XJM1Ll0+W9HtJmyStkvS/O/ocEfFX4HqgAdhP0i6SvitphaS1kn4iqSE9/iRJKyVdIunPwM8k9ZB0qaQ/pOddLGmfdPuDJC2Q9Kqk5yV9qijuGyRdI2l+ut9jkvZP3/t1utmT6a+SqZL2kHSPpGZJr6XLw4qON1LSr9Nj3Z8e+8ai94+S9Jv0v9eTkiZ19LexyufEbyUjqRfwS+A+YBAwA7hJ0oGdOMwU4EjgYOAE4BjggyRX4FOB9a3sMw84UNKoonWfBm5Ol68DvhwRuwOHAosyfJaewBeBzcBy4NtpHGOBA4ChwGVFu+wN9Af2BaYDXwfOBk4G+gKfB96U1AdYkMY2KN3mXyUdUnSss4F/BPYA/hu4EiAijknfPzwidouIW0n+H/9Zet7hwBbgR0XHuhn4HbAn8C3gs0WfcSgwH/inNPb/DdwhaWBHfx+rcBHhhx/d8gBeIkmEG4oebwKPpO9/FPgz8L6ifW4BvpUuPwh8sei9zxX2TV8HcGzR62OBF4Cjio/ZRmw3Apely6OATcCu6esVwJeBvh0c43PAtvRzvQI8CkwGBLwB7F+07QTgj+nyJOAtoHfR+88Dp7dyjqnAwy3W/Rtwebp8A3Bt0XsnA8+1+Bsd0M5nGAu8li4PTz/Pri3+Tjemy5cA/9Fi/18B08r9b82PnXv4it+625SI6Fd4AF8tem8I8KdImkkKXia5Os7qT4WFiFhEcvV6DbBW0mxJfdvY72aSK2VIrvbnRsSb6ev/SZJAX5b0kKQJ7Zz/0fSzDYiIoyLifmAgsCuwOG0S2QDcm64vaI6IrUWv9wH+0Mrx9wWOLBwnPdY5JL8YCv5ctPwmsFtbwUraVdK/SXpZ0kbg10A/ST1I/nu8WvR3gKK/bxrLmS1i+QgwuK3zWXVw4rdSWg3sI6n4391wYFW6/AZJAi0oTnYFO5STjYgfRsSHgUNImloubuPc9wEDJI0l+QIoNPMQEY9HxOkkTStzgdsyfp6CV0iaUA4p+tL7QCSdwK3GTZJg92/lWH8CHir+8oyk2eYrnYyp4G+BA4EjI6IvSdMYJL9S1gD9JRX/zfdpEct/tIilT0TM6mIsViGc+K2UHiNJ7n8nqVfaUXgq8PP0/SeAM9Kr1AOAL7R3MElHSDoy7Tt4A9gKbG9t24jYBtwOfIekvXpBeoz3SzpH0gci4m1gY1vHaEv6C+anwPclDUqPO1TSie3sdi3wfySNSjupD5O0J3AP8EFJn03/Rr3Szzk6Yzhrgf2KXu9O8qW0Qcnoo8uL4n4ZaAK+lf4dJpD89yi4EThV0olpZ3TvtKN6GFbVnPitZCLiLeA04CSSq+R/Bf5XRDyXbvJ9krbwtcAc4KYODtmXJOG+RtJktB74bjvb30zSJv+L9Iug4LPAS2lTyHnAZzrxsQouIelofTQ9zv0kV9pt+R7JL4v7SL5srgMaImITSaf1WSS/kP5M0nG8S8Y4vgXMSZtmPgX8C8nIo0KfxL0ttj+HpD9iPUkn7q3AXwAi4k/A6cClQDPJL4CLcd6oeorwRCxmlpB0K0ln8eUdbmxVy9/cZnUsbUbaX9L7JH2c5Ap/bpnDspz5DkKz+rY3cCfJOP6VwFciYml5Q7K8uanHzKzOuKnHzKzOVEVTz4ABA2LEiBHlDsPMrKosXrz4lYh4T4mNqkj8I0aMoKmpqeMNzczsHZJebm29m3rMzOqME7+ZWZ1x4jczqzNV0cbfmrfffpuVK1eydevWjjeuA71792bYsGH06tWr3KGYWYWr2sS/cuVKdt99d0aMGIGkcodTVhHB+vXrWblyJSNHjix3OGZW4ao28W/dutVJPyWJPffck+bm5nKHYmZtmLt0Fd/51fOs3rCFIf0auPjEA5kyrjNTUXSfqk38gJN+Ef8tzCrX3KWr+MadT7Pl7aTi96oNW/jGnU8DlCX5u3PXzCxn3/nV8+8k/YItb2/nO796vizxOPHvpCuvvJJDDjmEww47jLFjx/LYY4+VOyQzqzCrN2zp1Pq8VXVTT2fk0b7229/+lnvuuYclS5awyy678Morr/DWW291+Xjbtm2jZ8+6+U9iVjeG9GtgVStJfki/hjJEUydX/IX2tVUbthC82742d+mqDvdtz5o1axgwYAC77JJMjjRgwACGDBnC448/ztFHH83hhx/O+PHj2bRpE1u3buXcc89lzJgxjBs3jgceeACAG264gTPPPJNTTz2VE044gTfeeIPPf/7zHHHEEYwbN467774bgGeffZbx48czduxYDjvsMJYvX75TsZtZ6Vx84oE09Oqxw7qGXj24+MT2JmnLT11cXrbXvrYzV/0nnHACV1xxBR/84AeZPHkyU6dOZcKECUydOpVbb72VI444go0bN9LQ0MAPfvADAJ5++mmee+45TjjhBF544QUg+eXw1FNP0b9/fy699FKOPfZYrr/+ejZs2MD48eOZPHkyP/nJT7jwwgs555xzeOutt9i+vVPTwppZGRXyjEf1lFBe7Wu77bYbixcv5uGHH+aBBx5g6tSpfPOb32Tw4MEcccQRAPTt2xeARx55hBkzZgBw0EEHse+++76T+I8//nj69+8PwH333ce8efP47neTqWO3bt3KihUrmDBhAldeeSUrV67kjDPOYNSoUTsVu5mV1pRxQ8uW6Fuqi8SfZ/tajx49mDRpEpMmTWLMmDFcc801rQ6tbG/Cmz59+uyw3R133MGBB+74E3D06NEceeSRzJ8/nxNPPJFrr72WY489dqfjN7P6Uxdt/Hm1rz3//PM7tLU/8cQTjB49mtWrV/P4448DsGnTJrZt28YxxxzDTTfdBMALL7zAihUr3pPcAU488USuvvrqd74oli5NZsF78cUX2W+//bjgggs47bTTeOqpp3YqdjOrX3VxxZ9X+9rmzZuZMWMGGzZsoGfPnhxwwAHMnj2bc889lxkzZrBlyxYaGhq4//77+epXv8p5553HmDFj6NmzJzfccMM7ncLF/uEf/oGLLrqIww47jIhgxIgR3HPPPdx6663ceOON9OrVi7333pvLLrtsp2I3s/pVFXPuNjY2RsuJWJYtW8bo0aPLFFFl8t/EzIpJWhwRjS3X10VTj5mZvSvXxC/pQknPSHpW0kXpuv6SFkhanj7vkWcMZma2o9wSv6RDgS8B44HDgVMkjQJmAgsjYhSwMH3dJdXQTFUq/luYWVZ5XvGPBh6NiDcjYhvwEPBJ4HRgTrrNHGBKVw7eu3dv1q9f74THu/X4e/fuXe5QzKwK5Dmq5xngSkl7AluAk4EmYK+IWAMQEWskDWptZ0nTgekAw4cPf8/7w4YNY+XKla5BnyrMwGVm1pHcEn9ELJP0bWABsBl4EtjWif1nA7MhGdXT8v1evXp5tikzsy7IdRx/RFwHXAcg6Z+BlcBaSYPTq/3BwLo8YzAzq0Z5ztiV96ieQenzcOAM4BZgHjAt3WQacHeeMZiZVZu8KgoX5D2O/w5Jvwd+CZwfEa8Bs4DjJS0Hjk9fm5lZKu8Zu/Ju6vloK+vWA8fleV4zs2qW94xdvnPXzKzCtFU5uLtm7HLiN7OqN3fpKibOWsTImfOZOGtRt7WFl0veM3bVRXVOM6tdhY7QQpt4oSMUqJiJTzor7xm7nPjNrKrlNbVqueU5Y5cTv5lVneIx7m0VbemujtBa5MRvZlWlZdNOW7qrI7QWuXPXzKpKa007LXVnR2gt8hW/mVWV9ppwBN3eEVqLnPjNrKoM6dfAqlaS/9B+DfzXzGPLEFH1cVOPmVWVvMe41wNf8ZtZVcl7jHs9cOI3s6qT5xj3euCmHjOzOuPEb2ZWZ9zUY2ZWJM+ZryqFE7+ZWaoWC761Ju+pF78m6VlJz0i6RVJvSf0lLZC0PH3eI88YzMyyynvmq0qRW+KXNBS4AGiMiEOBHsBZwExgYUSMAhamr83Myi7vma8qRd6duz2BBkk9gV2B1cDpwJz0/TnAlJxjMDPLJO+ZrypFbok/IlYB3wVWAGuA1yPiPmCviFiTbrMGGNTa/pKmS2qS1NTc3JxXmGZm76iXu4LzbOrZg+TqfiQwBOgj6TNZ94+I2RHRGBGNAwcOzCtMM7N3TBk3lKvOGMPQfg2IpP7PVWeMqamOXch3VM9k4I8R0Qwg6U7gaGCtpMERsUbSYGBdjjGYmXVKPdwVnGcb/wrgKEm7ShJwHLAMmAdMS7eZBtydYwxmZtZCblf8EfGYpNuBJcA2YCkwG9gNuE3SF0i+HM7MKwYzM3uvXG/giojLgctbrP4LydW/mZmVgWv1mJnVGZdsMLMuq4e6NrXIid/MuqRe6trUIjf1mFmX1Etdm1rkK34z65J6qWtTrFaatnzFb2ZdUi91bQoKTVurNmwheLdpa+7SVeUOrdOc+M2sS+qlrk1BLTVtuanHzLqk0MRRC00fWdRS05YTv5l1WT3UtSkY0q+BVa0k+Wps2nJTj5lZBrXUtOUrfjOzDGqpacuJ38wso1pp2nJTj5lZncmU+CV9RNK56fJASSPzDcvMzPLSYeKXdDlwCfCNdFUv4MY8gzIzs/xkueL/JHAa8AZARKwGds8zKDMzy0+WxP9WRAQQAJL6ZDmwpAMlPVH02CjpIkn9JS2QtDx93mNnPoCZmXVOlsR/m6R/A/pJ+hJwP/DTjnaKiOcjYmxEjAU+DLwJ3AXMBBZGxChgYfrazMxKpN3hnOkk6bcCBwEbgQOByyJiQSfPcxzwh4h4WdLpwKR0/RzgQZI+BDMzK4F2E39EhKS5EfFhoLPJvthZwC3p8l4RsSY9/hpJg3biuGZWI2ql5HE1yNLU86ikI7p6AknvJ+kc/kUn95suqUlSU3Nzc1dPb2ZVoJZKHleDLIn/YyTJ/w+SnpL0tKSnOnGOk4AlEbE2fb1W0mCA9HldaztFxOyIaIyIxoEDB3bidGZWbWqp5HE1yFKy4aSdPMfZvNvMAzAPmAbMSp/v3snjm1mVq6WSx9Wgwyv+iHgZ6Aecmj76pes6JGlX4HjgzqLVs4DjJS1P35vVyZjNrMbU22xe5Zblzt0LgZuAQenjRkkzshw8It6MiD0j4vWidesj4riIGJU+v9rV4M0sMXfpKibOWsTImfOZOGtR1bWN11LJ42qQpannC8CREfEGgKRvA78Frs4zMDPLptAxWmgjL3SMAlUzKqaWSh5XgyyJX0Bxr8v2dJ2ZVYD2OkarKXHWSsnjapAl8f8MeEzSXenrKcB1uUVkZp3ijlHrrA4Tf0R8T9KDwEdIrvTPjYileQdmZtnU0lywVhpZOnePApZHxA8j4gfAf0s6Mv/QzCwLd4xaZ2W5gevHwOai12+k68ysAkwZN5SrzhjD0H4NCBjar4Grzhjj9nJrU6bO3bQsMwAR8VdJnqvXrIK4Y9Q6I8sV/4uSLpDUK31cCLyYd2BmZpaPLIn/POBoYFX6OBKYnmdQZmaWnyyjetaRlFU2M7Ma0OYVv6QvSRqVLkvS9ZJeTyt0fqh0IZqZWXdqr6nnQuCldPls4HBgP+DrwA/yDcvMzPLSXlPPtoh4O10+Bfj3iFgP3C/p/+YfmpkV8wxV1l3au+L/q6TBknqTzJl7f9F7viXQrIQ8Q5V1p/YS/2VAE0lzz7yIeBZA0v/AwznNSsozVFl3arOpJyLukbQvsHtEvFb0VhMwNffIzOwdLsRm3andcfwRsa1F0ici3oiIzW3tY2bdzzNUWXfKcgNXl0nqJ+l2Sc9JWiZpgqT+khZIWp4+75FnDGaVpKszZbkQm3WnXBM/ybDPeyPiIJLhoMuAmcDCiBgFLExfm9W8nemgdSE2604qqr/W+gaSgHOA/SLiCknDgb0j4ncd7NcXeDLdL4rWPw9Miog1kgYDD0ZEu5ctjY2N0dTUlO0TmVWoibMWtVo3f2i/Bv5r5rFliMhqnaTFEdHYcn2WK/5/BSaQ3MQFsAm4JsN++wHNwM8kLZV0raQ+wF4RsQYgfR7URsDTJTVJampubs5wOrPK5g5aqxRZEv+REXE+sBUg7ex9f4b9egIfAn4cEeNI6vhnbtaJiNkR0RgRjQMHDsy6m1nFcgetVYosif9tST2AAJA0EPhrhv1WAisj4rH09e0kXwRr0yYe0ud1nY7arAq5g9YqRZbE/0PgLmCQpCuBR4B/7miniPgz8CdJhX/VxwG/B+YB09J104C7Oxu0WTVyB61Vig47dwEkHUSSuEUyImdZpoNLY4FrSZqGXgTOJfmyuQ0YDqwAzoyIV9s7jjt3zcw6r63O3Q7r8UvqT9Icc0vRul5FBdzaFBFPAO85KcmXiJmZlUGWpp4lJKNzXgCWp8t/lLRE0ofzDM7MzLpflsR/L3ByRAyIiD2Bk0iaar5KMtTTzMyqSJbE3xgRvyq8iIj7gGMi4lFgl9wiMzOzXHTYxg+8KukS4Ofp66nAa+kQzyzDOs3MrIJkueL/NDAMmEsy9HJ4uq4H8KncIjMzs1x0eMUfEa8AM9p4+7+7NxwzM8tbluGcA4G/Aw4BehfWR4SrSllN8Fy2Vm+yNPXcBDwHjAT+kWQqxsdzjMmsZDyXrdWjLIl/z4i4Dng7Ih6KiM8DR+Ucl1lJeC5bq0dZRvUU7tBdI+kTwGqSzl6zqudSyVaPsiT+f5L0AeBvgauBvsBFeQZlVipD+jW0OjmKSyVbLcvS1PNaRLweEc9ExMci4sNAu0XVzKqFSyVbPcqS+K/OuM6s6rhUstWjNpt6JE0AjgYGSvp60Vt9SW7eMqsJU8YNdaK3utJeG//7gd3SbXYvWr8R+Js8gzIzs/y0mfgj4iHgIUk3RMTLJYzJzMxylGVUzy6SZgMjirfPcueupJeATcB2YFtENKYTu9yaHu8l4FPpBO5mZlYCWRL/L4CfkEyhuL2DbVvzsbTeT8FMkukbZ0mamb6+pAvHNTOzLsiS+LdFxI+78ZynA5PS5TnAgzjxm5mVTJbhnL+U9FVJgyX1LzwyHj+A+yQtljQ9XbdXRKwBSJ8HtbajpOmSmiQ1NTc3ZzydmZl1JMsV/7T0+eKidQHsl2HfiRGxWtIgYIGk57IGFhGzgdkAjY2NkXU/MzNrX5Z6/CO7evCIWJ0+r5N0FzAeWCtpcESskTQYWNfV45uZWed12NQjaVdJf5+O7EHSKEmnZNivj6TdC8vACcAzwDze/RUxjWRWLzMzK5EsTT0/AxaT3MULsJJkpM89Hey3F3CXpMJ5bo6IeyU9Dtwm6QvACuDMrgRuZmZdkyXx7x8RUyWdDRARW5Rm8/ZExIvA4a2sXw8c1+lIzSqAZ+uyWpAl8b8lqYGkQxdJ+wN/yTUqswpUmK2rMHFLYbYuwMnfqkqW4ZyXA/cC+0i6CVhIMgevWV3xbF1WK7KM6lkgaQnJdIsCLmxxJ65ZXfBsXVYrsozq+STJ3bvzI+IeYJukKblHZlZh2pqVy7N1WbXJ1NQTEa8XXkTEBpLmH7O64tm6rFZk6dxt7cshy35mNaXQgetRPVbtsiTwJknfA64hGdkzg2Rcv1nd8WxdVguyNPXMAN4iqaF/G7AFOD/PoMzMLD/tXvFL6gHcHRGTSxSPmZnlrN0r/ojYDrwp6QMlisfMzHKWpY1/K/C0pAXAG4WVEXFBblGZmVlusiT++enDzMxqQJY7d+ektXqGR4TvTa9DLkxmVluy3Ll7KvAESb0eJI2VNC/nuKxCFAqTrdqwheDdwmRzl64qd2hm1kVZhnN+i2TmrA0AEfEE0OVZuay6VGthsrlLVzFx1iJGzpzPxFmL/EVlViRLG/+2iHi9RQl+z4FbJ6qxMJnLJ5u1L8sV/zOSPg30SKddvBr4TdYTSOohaamke9LX/SUtkLQ8fd6ji7FbCVRjYbJq/ZViVipZ79w9hGTylZuB14GLOnGOC4FlRa9nAgsjYhRJbf+ZnTiWlVg1Fiarxl8pZqXUZlOPpN7AecABwNPAhIjY1pmDSxoGfAK4Evh6uvp0YFK6PAd4ELikM8e10qnGwmRD+jWwqpUkX8m/UsxKqb02/jnA28DDwEnAaDp3pQ/wLySzde1etG6viFgDEBFrJA3q5DGtxKqtMNnFJx64Qxs/VP6vFLNSai/xHxwRYwAkXQf8rjMHlnQKsC4iFkua1NnAJE0HpgMMHz68s7tbHavGXylmpdRe4n+7sBAR21qM6sliInCapJOB3kBfSTcCayUNTq/2BwPrWts5ImYDswEaGxs9isg6pdp+pZiVUnudu4dL2pg+NgGHFZYlbezowBHxjYgYFhEjgLOARRHxGWAeMC3dbBpw905+BjMz64Q2r/gjokdb7+2kWcBtkr4ArADOzOk8ZmbWipJMoRgRD5KM3iEi1gPHleK8Zmb2XlnG8ZuZWQ1x4jczqzNO/GZmdcaJ38yszpSkc9eswJO6mJWfE7+VjMslm1UGN/VYybhcslllcOK3knG5ZLPK4MRvJVONk7qY1SInfiuZapzUxawWuXPXSsblks0qgxO/lZTLJZuVn5t6zMzqjBO/mVmdceI3M6szTvxmZnXGid/MrM7kNqpHUm/g18Au6Xluj4jLJfUHbgVGAC8Bn4qI1/KKo5a0V+CsXMXPXHTNrPrkOZzzL8CxEbFZUi/gEUn/DzgDWBgRsyTNBGYCl+QYR01or8AZUJbiZy66ZladcmvqicTm9GWv9BHA6cCcdP0cYEpeMdSS9gqclav4mYuumVWnXNv4JfWQ9ASwDlgQEY8Be0XEGoD0eVAb+06X1CSpqbm5Oc8wq0J7Bc7KVfzMRdfMqlOuiT8itkfEWGAYMF7SoZ3Yd3ZENEZE48CBA3OLsVq0V+CsXMXPXHTNrDqVZFRPRGwAHgQ+DqyVNBggfV5XihiqXXsFzspV/MxF18yqU56jegYCb0fEBkkNwGTg28A8YBowK32+O68YakmWAmelHl3jomtm1UkRkc+BpcNIOm97kPyyuC0irpC0J3AbMBxYAZwZEa+2d6zGxsZoamrKJU4zs1olaXFENLZcn9sVf0Q8BYxrZf164Li8zms7z2PzzWqbyzLbDjw236z2uWSD7cBj881qnxO/7cBj881qnxO/7cBj881qnxN/jZi7dBUTZy1i5Mz5TJy1iLlLV3XpOB6bb1b73LlbA7qzQ9Zj881qnxN/NyvHUMj2OmS7cm5PiG5W25z4u1G5hkK6Q9bMOsNt/N2oXEMh3SFrZp3hxN+NynXl7Q5ZM+sMJ/5uVK4r7ynjhnLVGWMY2q8BAUP7NXDVGWPcTm9mrXIbfze6+MQDd2jjh9JdebtD1syycuLvRh4KaWbVwIm/m/nK28wqnRN/FXG5ZDPrDk78VcLlks2su+Q2qkfSPpIekLRM0rOSLkzX95e0QNLy9HmPvGLoqu6qe9OdXC7ZzLpLnsM5twF/GxGjgaOA8yUdDMwEFkbEKGBh+rpiFK6sV23YQvDulXW5k7/vzjWz7pJb4o+INRGxJF3eBCwDhgKnk8zFS/o8Ja8YuqJSr6x9d66ZdZeS3MAlaQTJ/LuPAXtFxBpIvhyAQW3sM11Sk6Sm5ubmUoQJVO6Vte/ONbPuknvil7QbcAdwUURszLpfRMyOiMaIaBw4cGB+AbZQqVfWvjvXzLpLrqN6JPUiSfo3RcSd6eq1kgZHxBpJg4F1ecbQWeW8+7YjvkfAzLpDnqN6BFwHLIuI7xW9NQ+Yli5PA+7OK4au8JW1mdU6RUQ+B5Y+AjwMPA38NV19KUk7/23AcGAFcGZEvNresRobG6OpqSmXOM3MapWkxRHR2HJ9bk09EfEIoDbePi6v8xb4Llczs9bV5J27vsvVzKxtNVmPv1LH4puZVYKaTPyVOhbfzKwS1GTir9Sx+GZmlaAmE7/vcjUza1tNdu56Jiwzs7bVZOIH3+VqZtaWmmzqMTOztjnxm5nVGSd+M7M648RvZlZnnPjNzOpMbtU5u5OkZuDljJsPAF7JMZyuclzZVWJMUJlxVWJMUJlxVWJMkG9c+0bEe2ayqorE3xmSmlorQ1pujiu7SowJKjOuSowJKjOuSowJyhOXm3rMzOqME7+ZWZ2pxcQ/u9wBtMFxZVeJMUFlxlWJMUFlxlWJMUEZ4qq5Nn4zM2tfLV7xm5lZO5z4zczqTM0kfknXS1on6Zlyx1JM0j6SHpC0TNKzki6sgJh6S/qdpCfTmP6x3DEVSOohaamke8odS4GklyQ9LekJSU3ljqdAUj9Jt0t6Lv33NaHM8RyY/o0Kj42SLipnTAWSvpb+W39G0i2SeldATBem8Txb6r9TzbTxSzoG2Az8e0QcWu54CiQNBgZHxBJJuwOLgSkR8fsyxiSgT0RsltQLeAS4MCIeLVdMBZK+DjQCfSPilHLHA0niBxojoqJu/pE0B3g4Iq6V9H5g14jYUOawgOQLHFgFHBkRWW++zCuWoST/xg+OiC2SbgP+MyJuKGNMhwI/B8YDbwH3Al+JiOWlOH/NXPFHxK+BV8sdR0sRsSYilqTLm4BlQFknCojE5vRlr/RR9isAScOATwDXljuWSiepL3AMcB1ARLxVKUk/dRzwh3In/SI9gQZJPYFdgdVljmc08GhEvBkR24CHgE+W6uQ1k/irgaQRwDjgsTKHUmhSeQJYByyIiLLHBPwL8HfAX8scR0sB3CdpsaTp5Q4mtR/QDPwsbRq7VlKfcgdV5CzglnIHARARq4DvAiuANcDrEXFfeaPiGeAYSXtK2hU4GdinVCd34i8RSbsBdwAXRcTGcscTEdsjYiwwDBif/vQsG0mnAOsiYnE542jDxIj4EHAScH7arFhuPYEPAT+OiHHAG8DM8oaUSJudTgN+Ue5YACTtAZwOjASGAH0kfaacMUXEMuDbwAKSZp4ngW2lOr8Tfwmk7eh3ADdFxJ3ljqdY2jzwIPDx8kbCROC0tD3958Cxkm4sb0iJiFidPq8D7iJply23lcDKol9qt5N8EVSCk4AlEbG23IGkJgN/jIjmiHgbuBM4uswxERHXRcSHIuIYkmbqkrTvgxN/7tKO1OuAZRHxvXLHAyBpoKR+6XIDyf8Yz5Uzpoj4RkQMi4gRJM0EiyKirFdlAJL6pJ3ypE0pJ5D8TC+riPgz8CdJB6arjgPKNmCghbOpkGae1ArgKEm7pv8/HkfS11ZWkgalz8OBMyjh36xmJluXdAswCRggaSVweURcV96ogORK9rPA02mbOsClEfGf5QuJwcCcdOTF+4DbIqJihk9WmL2Au5J8QU/g5oi4t7whvWMGcFPatPIicG6Z4yFtrz4e+HK5YymIiMck3Q4sIWlOWUpllG+4Q9KewNvA+RHxWqlOXDPDOc3MLBs39ZiZ1RknfjOzOuPEb2ZWZ5z4zczqjBO/mVmdceK3qiRpc4vXn5P0oxKe/yhJj6VVKJdJ+la6fpKkTt8cJOkGSX+TLl8r6eBO7DupkqqZWuWrmXH8Zt1BUo+I2J5h0znApyLiyfR+iMKNVJNIqsT+pqsxRMQXu7qvWRa+4reaI2lfSQslPZU+D0/Xv3NVnb7enD5PSudMuJnkRrs+kuan8xU8I2lqK6cZRFLwq1D36PdpEb7zgK+lvwQ+2s45JelHkn4vaX56vMI2D0pqTJdPkPRbSUsk/SKt+YSkjyupw/8IyV2fZpk58Vu1alDRpB/AFUXv/YhkXobDgJuAH2Y43njgmxFxMEndotURcXg6t0Nrd+p+H3he0l2Sviypd0S8BPwE+H5EjI2Ih9s53ydJfiWMAb5EK7VjJA0A/h6YnBaJawK+rmQSkZ8CpwIfBfbO8PnM3uHEb9VqS5pcx6ZVRi8rem8CcHO6/B/ARzIc73cR8cd0+WlgsqRvS/poRLzecuOIuIJkwpj7gE/T+pdDe44Bbkl/LawGFrWyzVHAwcB/pV9u04B9gYNIio4tj+TW+4ooZmfVw4nf6kGhLsk20n/zabGu9xdt88Y7G0e8AHyY5AvgKknFXyoUbfeHiPgxSdGvw9O6Ky21d86O6qWIZK6EwhfcwRHxhYz7mrXJid9q0W9IKnwCnEMy7R7ASyQJHZL67L1a21nSEODNiLiRZAKP95Q7lvSJNJEDjAK2AxuATcDuRZu2dc5fA2elE+IMBj7WSiiPAhMlHZCec1dJHySppDpS0v7pdme39jnM2uJRPVaLLgCul3QxySxVhaqVPwXulvQ7YCFFV/ktjAG+I+mvJJUTv9LKNp8Fvi/pTZKr+nMiYrukXwK3SzqdpHpmW+e8CziW5FfFCyRT7+0gIpolfQ64RdIu6eq/j4gXlMwENl/SKyRfbBUzz7RVPlfnNDOrM27qMTOrM078ZmZ1xonfzKzOOPGbmdUZJ34zszrjxG9mVmec+M3M6sz/B/Gsjpm5tF1KAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "df.plot(x='Hours', y='Scores', style='o')\n",
    "plt.title('Hours vs Percentage')\n",
    "plt.xlabel('Hours Studied')\n",
    "plt.ylabel('Percentage Score')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the graph above, we can clearly see that there is a positive linear relation between the number of hours studied and percentage of score."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Preparing the Data\n",
    "\n",
    "Now we have an idea about statistical details of our data. The next step is to divide the data into \"features\" and \"target\". Features are the independent variables while targets are dependent variables whose values are to be predicted. In our dataset we only have two columns. We want to predict the percentage score depending upon the hours studied. Therefore our feature set will consist of the \"Hours\" column, and the target will be the \"Score\" column. To extract the features and targets, execute the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = df.iloc[:, :-1]\n",
    "y = df.iloc[:, -1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Hours</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>5.1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3.2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>8.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>3.5</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Hours\n",
       "0    2.5\n",
       "1    5.1\n",
       "2    3.2\n",
       "3    8.5\n",
       "4    3.5"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    21\n",
       "1    47\n",
       "2    27\n",
       "3    75\n",
       "4    30\n",
       "Name: Scores, dtype: int64"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The features are stored in the X variable. We specified \"-1\" as the range for columns since we wanted our attribute set to contain all the columns except the last one, which is \"Scores\". Similarly the y variable contains the target. We specified 1 for the target column since the index for \"Scores\" column is -1.\n",
    "\n",
    "Now that we have our features and target, the next step is to split this data into training and test sets. We'll do this by using Scikit-Learn's built-in train_test_split() method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above script splits 80% of the data to training set while 20% of the data to test set. The test_size variable is where we actually specify the proportion of test set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Training the Algorithm\n",
    "\n",
    "We have split our data into training and testing sets, and now is finally the time to train our algorithm. Execute following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "LinearRegression()"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sklearn.linear_model import LinearRegression\n",
    "regressor = LinearRegression()\n",
    "regressor.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With Scikit-Learn it is extremely straight forward to implement linear regression models, as all you really need to do is import the LinearRegression class, instantiate it, and call the fit() method along with our training data.\n",
    "\n",
    "In the theory section we said that linear regression model basically finds the best value for the intercept and slope, which results in a line that best fits the data. To see the value of the intercept and slop calculated by the linear regression algorithm for our dataset, execute the following code.\n",
    "\n",
    "To retrieve the intercept:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.018160041434683\n"
     ]
    }
   ],
   "source": [
    "print(regressor.intercept_)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For retrieving the slope (coefficient of x):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[9.91065648]\n"
     ]
    }
   ],
   "source": [
    "print(regressor.coef_)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that for every one unit of change in hours studied, the change in the score is about 9.91%. Or in simpler words, if a student studies one hour more than they previously studied for an exam, they can expect to achieve an increase of 9.91% in the score achieved by the student previously."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Making Predictions\n",
    "\n",
    "Now that we have trained our algorithm, it's time to make some predictions. To do so, we will use our test data and see how accurately our algorithm predicts the percentage score. To make predictions on the test data, execute the following script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Hours</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>1.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3.2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>7.4</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>2.5</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>5.9</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "    Hours\n",
       "5     1.5\n",
       "2     3.2\n",
       "19    7.4\n",
       "16    2.5\n",
       "11    5.9"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "X_test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([16.88414476, 33.73226078, 75.357018  , 26.79480124, 60.49103328])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y_pred = regressor.predict(X_test)\n",
    "y_pred"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The y_pred is a numpy array that contains all the predicted values for the input values in the X_test series."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Actual</th>\n",
       "      <th>Predicted</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>20</td>\n",
       "      <td>16.884145</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>27</td>\n",
       "      <td>33.732261</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>69</td>\n",
       "      <td>75.357018</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>30</td>\n",
       "      <td>26.794801</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>62</td>\n",
       "      <td>60.491033</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "    Actual  Predicted\n",
       "5       20  16.884145\n",
       "2       27  33.732261\n",
       "19      69  75.357018\n",
       "16      30  26.794801\n",
       "11      62  60.491033"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Though our model is not very precise, the predicted percentages are close to the actual ones."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:**\n",
    "\n",
    "The values in the columns above may be different in your case because the train_test_split function randomly splits data into train and test sets, and your splits are likely different from the one shown in this article."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Evaluating the Model's Perfomance\n",
    "\n",
    "The final step is to evaluate the performance of algorithm. This step is particularly important to compare how well different algorithms perform on a particular dataset. For regression algorithms, in particular these three evaluation metrics are commonly used:\n",
    "\n",
    "Mean Absolute Error (MAE) is the mean of the absolute value of the errors. It is calculated as:\n",
    "\n",
    "![Mean Absolute Error](https://s3.amazonaws.com/stackabuse/media/linear-regression-python-scikit-learn-3.png)\n",
    "\n",
    "Mean Squared Error (MSE) is the mean of the squared errors and is calculated as:\n",
    "\n",
    "![Mean Squared Error](https://s3.amazonaws.com/stackabuse/media/linear-regression-python-scikit-learn-4.png)\n",
    "\n",
    "Root Mean Squared Error (RMSE) is the square root of the mean of the squared errors:\n",
    "\n",
    "\n",
    "![Root Mean Squared Error](https://s3.amazonaws.com/stackabuse/media/linear-regression-python-scikit-learn-5.png)\n",
    "\n",
    "Luckily, we don't have to perform these calculations manually. The Scikit-Learn library comes with pre-built functions that can be used to find out these values for us."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's find the values for these metrics using our test data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mean Absolute Error: 4.183859899002975\n",
      "Mean Squared Error: 21.5987693072174\n",
      "Root Mean Squared Error: 4.6474476121003665\n"
     ]
    }
   ],
   "source": [
    "from sklearn import metrics \n",
    "import numpy as np\n",
    "\n",
    "print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))  \n",
    "print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))  \n",
    "print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:aiss] *",
   "language": "python",
   "name": "conda-env-aiss-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
